# Pencil Durability Kata

Tommy Gundy  
December 11, 2018  
Completed for Pillar Technologies

# Object Modeling & Design Considerations

One of the more challenging aspects of this problem was the Edit() method. It was ambiguous whether editing was holistic, involving the entire real-world act of erasing and replacing text, or if it was merely filling in whitespace left by the eraser. Initially, I implemented this method under the former assumption, but the method was long and cumbersome. I opted for an overload to cover both functionalities, which had the added benefit of allowing me to make the initial method more readable by simply calling the new overload method at an index determined by the remaining durability of the eraser.

Otherwise, I had to decide which models to create. Scoping everything into a single Pencil class likely would have been sufficient, but erasers are real items that happen to be miniaturized and tacked on to the back of pencils, so I decided to make Eraser its own class with an object instance being created inside of Pencil's constructor. I weighed adding a Paper class, and with expanded functionality I think this would make a lot of sense, but I could only think of it having a single string property representing its text, so I left it out.

Finally, I struggled to adequately cover for edge cases that involved a string index throwing an out of bounds exception. My best solution was the somewhat clumsy but nevertheless effective if statement to return from the method "early" in those cases.

Thank you for reviewing my kata. The design was often challenging (the various refactorings reflected in my commits are evidence of this) and I enjoyed the experience.

# Building and Running Tests

This kata was created in Visual Studio 2017. Besides the command line, all tests can be run inside of the Test Manager if desired. There are no external dependencies to run the project.

After cloning this Kata's repository, to run the tests, enter `dotnet test PencilDurability/PencilDurabilityTests/PencilDurabilityTests.csproj` in the command line from inside the cloned repository's directory on your machine.

Running `dotnet test -v n PencilDurability/PencilDurabilityTests/PencilDurabilityTests.csproj` will allow each test to show up by name.