﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PencilDurability
{
    public class Eraser
    {
        public int Durability{ get; set; }

        public Eraser(int durability)
        {
            Durability = durability;
        }

        public string Erase(string paper, string text)
        {
            int index = paper.LastIndexOf(text);

            if (index < 0)
            {
                return paper;
            }

            int length = GetEraseableTextLength(text);

            return Replace(paper, (index + text.Length - length), new string(' ', length));
        }

        public int GetEraseableTextLength(string text)
        {
            int count = 0;

            for (int i = text.Length - 1; i >= 0; i--)
            {
                bool successfullyErased = DegradeEraser(text[i] == '\n' || text[i] == ' ' ? 0 : 1);
                count += successfullyErased ? 1 : 0;
            }

            return count;
        }

        public bool DegradeEraser(int degradationValue)
        {
            if (Durability >= degradationValue)
            {
                Durability--;
                return true;
            }
            return false;
        }

        public string Replace(string paper, int index, string newText)
        {
            if (index < 0 || index >= paper.Length)
            {
                return paper;
            }
            return paper.Remove(index, newText.Length).Insert(index, newText);
        }
    }
}
