﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PencilDurability
{
    public class Pencil
    {
        public int PointDurability { get; set; }
        public int RemainingPoint { get; set; }
        public int Length { get; set; }
        public Eraser MiniEraser { get; set; }

        public Pencil(int pointDurability, int length, int eraserDurability)
        {
            PointDurability = pointDurability;
            RemainingPoint = pointDurability;
            Length = length;
            MiniEraser = new Eraser(eraserDurability);
        }

        public string Write(string paper, string text)
        {
            return paper + GetWriteableText(text);
        }

        public string GetWriteableText(string text)
        {
            string result = "";

            for (int i = 0; i < text.Length; i++)
            {
                bool successfullyWritten = DegradePoint(GetDegradationValue(text[i]));
                result += successfullyWritten ? text[i] : ' ';
            }

            return result;
        }

        public int GetDegradationValue(char character)
        {
            if (character == ' ' || character == '\n')
            {
                return 0;
            }
            return char.IsUpper(character) ? 2 : 1;
        }

        public bool DegradePoint(int degradationValue)
        {
            if (RemainingPoint >= degradationValue)
            {
                RemainingPoint -= degradationValue;
                return true;
            }
            return false;
        }

        public void Sharpen()
        {
            if (Length > 0)
            {
                Length--;
                RemainingPoint = PointDurability;
            }
        }

        public string Edit(string paper, string eraseText, string newText)
        {
            int index = paper.LastIndexOf(eraseText);

            if (index < 0)
            {
                return paper;
            }

            int length = MiniEraser.GetEraseableTextLength(eraseText);
            index += (eraseText.Length - length);
            paper = MiniEraser.Replace(paper, index, new string(' ', length));

            return Edit(paper, index, newText);
        }

        public string Edit(string paper, int index, string newText)
        {
            if (index < 0 || index >= paper.Length)
            {
                return paper;
            }

            newText = GetWriteableText(newText);

            for (int i = 0; i < newText.Length; i++)
            {
                if (index + i < paper.Length)
                {
                    char newCharacter = paper[index + i] == ' ' ? newText[i] : '@';
                    paper = MiniEraser.Replace(paper, index + i, newCharacter.ToString());
                }
                else
                {
                    paper += newText[i];
                }
            }

            return paper;
        }
    }
}
