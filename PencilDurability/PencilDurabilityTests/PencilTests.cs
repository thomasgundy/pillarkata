using Microsoft.VisualStudio.TestTools.UnitTesting;
using PencilDurability;

namespace PencilDurabilityTests
{
    [TestClass]
    public class PencilTests
    {
        private Pencil pencil;

        [TestInitialize]
        public void Initialize()
        {
            pencil = new Pencil(10, 5, 5);
        }

        // Write() Tests
        [TestMethod]
        public void WhenWritePassedPaperAndTextReturnsSumOfPaperAndText()
        {
            Assert.AreEqual("papertext newtext", pencil.Write("papertext", " newtext"));
        }

        [TestMethod]
        public void WhenWritePassedPaperAndOverflowTextReturnsSumOfPaperAndTruncatedTextWithSpaces()
        {
            Assert.AreEqual("papertext Hey Joe, s     ", pencil.Write("papertext", " Hey Joe, scram!"));
        }

        [TestMethod]
        public void WhenWritePassedEmptyPaperAndTextReturnsText()
        {
            Assert.AreEqual("Let's go!", pencil.Write("", "Let's go!"));
        }

        [TestMethod]
        public void WhenWritePassedPaperAndNoTextReturnsPaper()
        {
            Assert.AreEqual("Let's go!", pencil.Write("Let's go!", ""));
        }

        // GetDegradationValue() Tests
        [TestMethod]
        public void WhenGetDegradationValuePassedLowercaseReturns1()
        {
            Assert.AreEqual(1, pencil.GetDegradationValue('a'));
        }

        [TestMethod]
        public void WhenGetDegradationValuePassedUppercaseReturns2()
        {
            Assert.AreEqual(2, pencil.GetDegradationValue('A'));
        }

        [TestMethod]
        public void WhenGetDegradationValuePassedSpecialCharacterReturns1()
        {
            Assert.AreEqual(1, pencil.GetDegradationValue('\''));
            Assert.AreEqual(1, pencil.GetDegradationValue('?'));
            Assert.AreEqual(1, pencil.GetDegradationValue(','));
            Assert.AreEqual(1, pencil.GetDegradationValue('!'));
            Assert.AreEqual(1, pencil.GetDegradationValue('^'));
            Assert.AreEqual(1, pencil.GetDegradationValue('('));
        }

        [TestMethod]
        public void WhenGetDegradationValuePassedNumberReturns1()
        {
            Assert.AreEqual(1, pencil.GetDegradationValue('3'));
            Assert.AreEqual(1, pencil.GetDegradationValue('8'));
        }

        [TestMethod]
        public void WhenGetDegradationValuePassedWhitespaceReturns0()
        {
            Assert.AreEqual(0, pencil.GetDegradationValue(' '));
            Assert.AreEqual(0, pencil.GetDegradationValue('\n'));
        }

        // DegradePoint() Tests
        [TestMethod]
        public void WhenDegradePointPassedValueLessThanRemainingPointSucceedsAndReturnsTrue()
        {
            Assert.IsTrue(pencil.DegradePoint(2));
            Assert.AreEqual(8, pencil.RemainingPoint);
        }

        [TestMethod]
        public void WhenDegradePointPassedValueGreaterThanRemainingPointFailsAndReturnsFalse()
        {
            Assert.IsFalse(pencil.DegradePoint(12));
            Assert.AreEqual(10, pencil.RemainingPoint);
        }

        [TestMethod]
        public void WhenDegradePointPassedValueEqualToRemainingPointSucceedsAndReturnsTrue()
        {
            Assert.IsTrue(pencil.DegradePoint(10));
            Assert.AreEqual(0, pencil.RemainingPoint);
        }

        // GetWriteableText() Tests
        [TestMethod]
        public void WhenGetWriteableTextPassedTextThatCanBeFullyWrittenReturnsText()
        {
            Assert.AreEqual("nice", pencil.GetWriteableText("nice"));
        }

        [TestMethod]
        public void WhenGetWriteableTextPassedTextThatCannotBeFullyWrittenReturnsTruncatedTextWithSpaces()
        {
            Assert.AreEqual("I'm so\nsurp      ", pencil.GetWriteableText("I'm so\nsurprised!"));
        }

        [TestMethod]
        public void WhenGetWriteableTextPassedBlankTextReturnsTextAndDoesNotDegradePoint()
        {
            Assert.AreEqual("", pencil.GetWriteableText(""));
            Assert.AreEqual(10, pencil.RemainingPoint);
        }

        // Sharpen() Tests
        [TestMethod]
        public void WhenSharpenCalledOnGreaterThanZeroLengthPencilRestoresPointDurability()
        {
            pencil.Sharpen();
            Assert.AreEqual(pencil.PointDurability, pencil.RemainingPoint);
            Assert.AreEqual(4, pencil.Length);
        }

        [TestMethod]
        public void WhenSharpenCalledOnZeroLengthPencilFailsAndDoesNotDegradeLength()
        {
            Pencil shortPencil = new Pencil(10, 0, 5);
            shortPencil.Write("", "my oh my");
            shortPencil.Sharpen();
            Assert.AreEqual(4, shortPencil.RemainingPoint);
            Assert.AreEqual(0, shortPencil.Length);
        }

        // Edit() Tests
        [TestMethod]
        public void WhenEditPassedEqualLengthStringsReturnsPaperWithReplacement()
        {
            Assert.AreEqual("I like pizza", pencil.Edit("I like tacos", "tacos", "pizza"));
        }

        [TestMethod]
        public void WhenEditPassedNoReplacementTextReturnsErasedPaper()
        {
            Assert.AreEqual("I like      ", pencil.Edit("I like tacos", "tacos", ""));
        }

        [TestMethod]
        public void WhenEditPassedEmptyPaperReturnsEmptyString()
        {
            Assert.AreEqual("", pencil.Edit("", "adore", "despise"));
        }

        [TestMethod]
        public void WhenEditPassedWhitespaceEraseTextReturnsPaperWithNewTextInWhitespace()
        {
            Assert.AreEqual("I like squirrels", pencil.Edit("I      squirrels", "     ", "like"));
        }

        [TestMethod]
        public void WhenEditPassedLongerReplacementTextReturnsPaperWithAtSigns()
        {
            Assert.AreEqual("I despi@@cos", pencil.Edit("I like tacos", "like", "despise"));
        }

        [TestMethod]
        public void WhenEditErases3AndWrites3PointAndEraserDegradeAccordingly()
        {
            pencil.Edit("I like racing", "ing", "ecars");
            Assert.AreEqual(2, pencil.MiniEraser.Durability);
            Assert.AreEqual(5, pencil.RemainingPoint);
        }

        [TestMethod]
        public void WhenEditPassedEraseTextWithNoMatchReturnsPaper()
        {
            Assert.AreEqual("I like tacos", pencil.Edit("I like tacos", "adore", "despise"));
        }

        [TestMethod]
        public void WhenEditPassedNewTextThatOverflowsPaperReturnsBiggerPaper()
        {
            Assert.AreEqual("I like racecars", pencil.Edit("I like racing", "ing", "ecars"));
        }

        // Edit() Overload Tests
        [TestMethod]
        public void WhenEdit2PassedNewTextAtWhiteSpaceIndexReturnsPaperWithNewText()
        {
            Assert.AreEqual("I shun racecars", pencil.Edit("I      racecars", 2, "shun"));
        }

        [TestMethod]
        public void WhenEdit2PassedNewTextPassingWhiteSpaceReturnsNewPaperWithAtSigns()
        {
            Assert.AreEqual("I manuf@@@@@@rs", pencil.Edit("I      racecars", 2, "manufacture"));
        }

        [TestMethod]
        public void WhenEdit2PassedNewTextThatOverflowsPaperReturnsBiggerPaper()
        {
            Assert.AreEqual("She surp@@@ed", pencil.Edit("She     him", 4, "surpassed"));
        }

        [TestMethod]
        public void WhenEdit2PassedIndexOutOfPaperRangeReturnsPaper()
        {
            Assert.AreEqual("He saw     ", pencil.Edit("He saw     ", -1, "it"));
            Assert.AreEqual("He saw     ", pencil.Edit("He saw     ", 100, "it"));
            Assert.AreEqual("He saw    it", pencil.Edit("He saw     ", 10, "it"));
        }

        [TestMethod]
        public void WhenEdit2Writes1Uppsercase5LowercasePointDegradeAccordingly()
        {
            pencil.Edit("      plays baseball", 0, "Jimmy");
            Assert.AreEqual(4, pencil.RemainingPoint);
        }
    }
}
