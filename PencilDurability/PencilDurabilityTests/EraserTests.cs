﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PencilDurability;

namespace PencilDurabilityTests
{
    [TestClass]
    public class EraserTests
    {
        private Eraser eraser;

        [TestInitialize]
        public void Initialize()
        {
            eraser = new Eraser(5);
        }

        // Erase() Tests
        [TestMethod]
        public void WhenEraseIsPassedPaperAndMatchingTextReturnsPaperWithWhitespaceOverText()
        {
            Assert.AreEqual("Jimmy      ", eraser.Erase("Jimmy Smith", "Smith"));
        }

        [TestMethod]
        public void WhenEraseIsPassedPaperAndNonMatchingTextReturnsPaper()
        {
            Assert.AreEqual("Jimmy Smith", eraser.Erase("Jimmy Smith", "Butler"));
        }

        [TestMethod]
        public void WhenEraseIsPassedPaperAndOverflowTextReturnPartialErasure()
        {
            Assert.AreEqual("Jimmy B     ", eraser.Erase("Jimmy Butler", "Butler"));
        }

        [TestMethod]
        public void WhenEraseIsPassedPaperAndTextWithNumbersAndSpecialCharactersReturnErasure()
        {
            Assert.AreEqual("Cat     ", eraser.Erase("Catch-22", "Catch-22"));
        }

        [TestMethod]
        public void WhenEraseIsPassedPaperWhereTextAppearsMoreThanOnceErasesOnlyLastInstance()
        {
            Assert.AreEqual("ChrisPaul    George", eraser.Erase("ChrisPaulPaulGeorge", "Paul"));
        }

        [TestMethod]
        public void WhenEraseIsPassedPaperAndEmptyTextReturnsPaperAndDoesNotDegradeEraser()
        {
            Assert.AreEqual("133-77", eraser.Erase("133-77", ""));
            Assert.AreEqual(5, eraser.Durability);
        }

        // DegradeEraser() Tests
        [TestMethod]
        public void WhenDegradeEraserPassedValueLessThanRemainingDegradesReturnsTrue()
        {
            Assert.IsTrue(eraser.DegradeEraser(1));
            Assert.AreEqual(4, eraser.Durability);
        }

        [TestMethod]
        public void WhenDegradeEraserPassedValueGreaterThanRemainingFailsReturnsFalse()
        {
            Assert.IsFalse(eraser.DegradeEraser(10));
            Assert.AreEqual(5, eraser.Durability);
        }

        [TestMethod]
        public void WhenDegradeEraserPassedValueEqualToRemainingDegradesReturnsTrue()
        {
            Assert.IsTrue(eraser.DegradeEraser(5));
            Assert.AreEqual(4, eraser.Durability);
        }

        // GetEraseableText() Tests
        [TestMethod]
        public void WhenGetEraseableTextLengthPassedStringLessThanEraserReturnsTextLength()
        {
            Assert.AreEqual(4, eraser.GetEraseableTextLength("nice"));
        }

        [TestMethod]
        public void WhenGetEraseableTextLengthPassedStringGreaterThanEraserReturnsDurability()
        {
            Assert.AreEqual(5, eraser.GetEraseableTextLength("onomatopoeia"));
        }

        [TestMethod]
        public void WhenGetEraseableTextLengthPassedBlankTextReturnsZeroAndDoesNotDegradeEraser()
        {
            Assert.AreEqual(0, eraser.GetEraseableTextLength(""));
            Assert.AreEqual(5, eraser.Durability);
        }

        // Replace() Tests
        [TestMethod]
        public void WhenReplacePassedWhitespaceReturnsPaperErasedAtThatIndex()
        {
            Assert.AreEqual("I love puppies", eraser.Replace("I hate puppies", 2, "love"));
        }

        [TestMethod]
        public void WhenReplacePassedIndexOutOfItsRangeReturnsPaper()
        {
            Assert.AreEqual("I hate puppies", eraser.Replace("I hate puppies", -1, "love"));
            Assert.AreEqual("I hate puppies", eraser.Replace("I hate puppies", 40, "love"));
        }
    }
}
